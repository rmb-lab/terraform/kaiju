terraform {
  backend "gcs" {
    bucket = "rmb-lab-terraform"
    credentials = "creds.json"
  }
}
