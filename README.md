# kaiju

Terraform to control all the things

## Requirements

* A gcp bucket to store state `rmb-lab-terraform`
* A gcp service account with the following access
    * `roles/storage.objectAdmin` on the bucket `rmb-lab-terraform`
    * `roles/cloudkms.cryptoKeyEncrypterDecrypter` on the key `sops-key` and keyring `sops`

# Cloudflare

Controls cloudflare stuff

## rmb938.me

Controls resources for the rmb938.me domain

# Kubernetes

Controls kubernetes stuff

## kube0

Controls resources for the kube0 cluster

# Secrets

Place secrets in the `secrets/encrypted/` folder.

**WARNING**: Make sure they are encrypted with GCP KMS ID `projects/rmb-lab/locations/global/keyRings/sops/cryptoKeys/sops-key`

Before terraform is ran make sure all secrets are decrypted by running `./decrypt.sh`
