#!/bin/sh
set -euxo pipefail

for filepath in secrets/encrypted/*; do
    filename=`basename $filepath`
    sops -d secrets/encrypted/$filename > secrets/decrypted/$filename
done
