locals {
  domain = "rmb938.me"
}

resource "cloudflare_record" "esxi1" {
    domain = "${local.domain}"
    name = "esxi1"
    type = "A"
    value = "192.168.23.1"
}

resource "cloudflare_record" "foreman" {
    domain = "${local.domain}"
    name = "foreman"
    type = "A"
    value = "192.168.23.50"
}

resource "cloudflare_record" "freenas" {
    domain = "${local.domain}"
    name = "freenas"
    type = "A"
    value = "192.168.23.40"
}

resource "cloudflare_record" "freenas-ipmi" {
    domain = "${local.domain}"
    name = "freenas-ipmi"
    type = "A"
    value = "192.168.20.1"
}

resource "cloudflare_record" "mirrors" {
    domain = "${local.domain}"
    name = "mirrors"
    type = "A"
    value = "192.168.23.41"
}

resource "cloudflare_record" "kube0_kubernetes" {
    domain = "${local.domain}"
    name = "kube0.kubernetes"
    type = "A"
    value = "192.168.23.60"
}

resource "cloudflare_record" "wildcard_kube0_kubernetes" {
    domain = "${local.domain}"
    name = "*.kube0.kubernetes"
    type = "CNAME"
    value = "${cloudflare_record.kube0_kubernetes.hostname}"
}
