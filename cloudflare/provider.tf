locals {
  cloudflare_creds = "${split("\n", file("secrets/decrypted/cloudflare"))}"
}


provider "cloudflare" {
  email = "${local.cloudflare_creds[0]}"
  token = "${local.cloudflare_creds[1]}"
}
