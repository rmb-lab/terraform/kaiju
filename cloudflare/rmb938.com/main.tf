locals {
  domain = "rmb938.com"
}

resource "cloudflare_record" "wildcard_kube0_kubernetes" {
    domain = "${local.domain}"
    name = "*.kube0.kubernetes"
    type = "CNAME"
    value = "pfsense.${local.domain}"
}
